
// Nate Peavey
// SNHU
// IT-312

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <time.h>
#include <stdlib.h>

#include "player.h"

using namespace std;

int main()
{
	// initialize variables
	int selection;
	int diceOne;
	int diceTwo;
	int diceThree;
	int chipNumber;
	int roundNumber = 1;
	bool mainMenu = true;
	bool gameOver = false;

	// set up rng seed
	srand((unsigned)time(NULL));

	// loop through main menu until game starts
	while (mainMenu == true)
	{
		// clear screen and output menu options
		system("cls");
		cout << "LCR Game" << endl;
		cout << "1. Begin Game" << endl;
		cout << "2. Game Rules" << endl;
		// take in users input
		cout << "Input: ";
		cin >> selection;

		// the second option is chosen read rules from text file and output them
		if (selection == 2)
		{
			system("cls");
			string line;
			ifstream myfile;
			myfile.open("gamerules.txt");
			while (getline(myfile, line))
			{
				cout << line << endl;
			}
			myfile.close();

			// pause screen for player to read rules
			system("pause");
		}

		// if first option is chosen initialize and start game loop
		else if (selection == 1)
		{
			mainMenu = false;

			// initialize player classes
			player playerOne(2, 1);
			player playerTwo(0, 2);
			player playerThree(1, 0);

			// put players into a vector
			vector<player> players{ playerOne, playerTwo, playerThree };

			// loop until the win condition happens
			while (gameOver == false)
			{
				// loop through players turns
				for (int i = 0; i < 3; ++i)
				{
					// output players chip numbers
					system("cls");
					cout << "Round " << roundNumber << endl << endl;
					cout << "Player 1's Number of Chips: " << players[0].getChipNumber() << endl;
					cout << "Player 2's Number of Chips: " << players[1].getChipNumber() << endl;
					cout << "Player 3's Number of Chips: " << players[2].getChipNumber() << endl;
					cout << endl;
					cout << "Player " << i + 1 << "'s turn" << endl;

					// roll dice
					diceOne = (rand() % 6) + 1;
					diceTwo = (rand() % 6) + 1;
					diceThree = (rand() % 6) + 1;

					// set current number of chips at start of turn to variable 
					chipNumber = players[i].getChipNumber();

					// if the player has no chips pass turn
					if (chipNumber == 0)
					{
						cout << "You are out of chips, next player's turn" << endl;
					}

					// if the player has 3 or more chips roll check dice one for result
					if (chipNumber > 2)
					{
						// if the die rolled a 1 pass one chip to the person on their left
						if (diceOne == 1)
						{
							cout << "You rolled a L, pass one chip to the player on your left." << endl;

							// use class variable to find which player is on the left and add one chip to their total
							players[players[i].getLeft()].setChipNumber(players[players[i].getLeft()].getChipNumber() + 1);

							// remove 1 chip
							players[i].setChipNumber(players[i].getChipNumber() - 1);
						}

						// if the die rolled a 2 discard one chip
						else if (diceOne == 2)
						{
							cout << "You rolled a C, deposit one chip into the center discard pot." << endl;

							// remove 1 chip
							players[i].setChipNumber(players[i].getChipNumber() - 1);
						}

						// if the die rolled a 3 pass one chip to the person on their right
						else if (diceOne == 3)
						{
							cout << "You rolled a R, pass on chip to the player on your right." << endl;

							// use class variable to find which player is on the right and add one chip to their total
							players[players[i].getRight()].setChipNumber(players[players[i].getRight()].getChipNumber() + 1);

							// remove 1 chip
							players[i].setChipNumber(players[i].getChipNumber() - 1);
						}

						// if you roll a 4 - 6 output message
						else if (diceOne == 4)
						{
							cout << "You rolled a 4, your chips are safe." << endl;
						}
						else if (diceOne == 5)
						{
							cout << "You rolled a 5, your chips are safe." << endl;
						}
						else if (diceOne == 6)
						{
							cout << "You rolled a 6, your chips are safe." << endl;
						}
					}

					// if the player has 2 of more chips roll this dice
					if (chipNumber > 1)
					{
						// if the die rolled a 1 pass one chip to the person on their left
						if (diceTwo == 1)
						{
							cout << "You rolled a L, pass one chip to the player on your left." << endl;

							// use class variable to find which player is on the left and add one chip to their total
							players[players[i].getLeft()].setChipNumber(players[players[i].getLeft()].getChipNumber() + 1);

							// remove 1 chip
							players[i].setChipNumber(players[i].getChipNumber() - 1);
						}

						// if the die rolled a 2 discard one chip
						else if (diceTwo == 2)
						{
							cout << "You rolled a C, deposit one chip into the center discard pot." << endl;

							// remove 1 chip
							players[i].setChipNumber(players[i].getChipNumber() - 1);
						}

						// if the die rolled a 3 pass one chip to the person on their right
						else if (diceTwo == 3)
						{
							cout << "You rolled a R, pass on chip to the player on your right." << endl;

							// use class variable to find which player is on the right and add one chip to their total
							players[players[i].getRight()].setChipNumber(players[players[i].getRight()].getChipNumber() + 1);

							// remove 1 chip
							players[i].setChipNumber(players[i].getChipNumber() - 1);
						}

						// if you roll a 4 - 6 output message
						else if (diceTwo == 4)
						{
							cout << "You rolled a 4, your chips are safe." << endl;
						}
						else if (diceTwo == 5)
						{
							cout << "You rolled a 5, your chips are safe." << endl;
						}
						else if (diceOne == 6)
						{
							cout << "You rolled a 6, your chips are safe." << endl;
						}
					}

					if (chipNumber > 0)
					{
						// if the die rolled a 1 pass one chip to the person on their left
						if (diceThree == 1)
						{
							cout << "You rolled a L, pass one chip to the player on your left." << endl;

							// use class variable to find which player is on the left and add one chip to their total
							players[players[i].getLeft()].setChipNumber(players[players[i].getLeft()].getChipNumber() + 1);

							// remove 1 chip
							players[i].setChipNumber(players[i].getChipNumber() - 1);
						}

						// if the die rolled a 2 discard one chip
						else if (diceThree == 2)
						{
							cout << "You rolled a C, deposit one chip into the center discard pot." << endl;

							// remove 1 chip
							players[i].setChipNumber(players[i].getChipNumber() - 1);
						}

						// if the die rolled a 3 pass one chip to the person on their right
						else if (diceThree == 3)
						{
							cout << "You rolled a R, pass on chip to the player on your right." << endl;

							// use class variable to find which player is on the right and add one chip to their total
							players[players[i].getRight()].setChipNumber(players[players[i].getRight()].getChipNumber() + 1);

							// remove 1 chip
							players[i].setChipNumber(players[i].getChipNumber() - 1);
						}

						// if you roll a 4 - 6 output message
						else if (diceThree == 4)
						{
							cout << "You rolled a 4, your chips are safe." << endl;
						}
						else if (diceThree == 5)
						{
							cout << "You rolled a 5, your chips are safe." << endl;
						}
						else if (diceThree == 6)
						{
							cout << "You rolled a 6, your chips are safe." << endl;
						}
					}

					// check to see if player 1 has won
					if (players[1].getChipNumber() == 0 && players[2].getChipNumber() == 0)
					{
						// clear screen, output winning message and end game loop
						system("cls");
						cout << "Player 1 wins!" << endl;
						gameOver = true;
						break;
					}

					// check to see if player 2 has won
					else if (players[0].getChipNumber() == 0 && players[2].getChipNumber() == 0)
					{
						// clear screen, output winning message and end game loop
						system("cls");
						cout << "Player 2 wins!" << endl;
						gameOver = true;
						break;
					}

					// check to see if player 3 has won
					else if (players[0].getChipNumber() == 0 && players[1].getChipNumber() == 0)
					{
						// clear screen, output winning message and end game loop
						system("cls");
						cout << "Player 3 wins!" << endl;
						gameOver = true;
						break;
					}
					system("pause");
				}
				// increase round number
				++roundNumber;
			}
		}
	}	
}

