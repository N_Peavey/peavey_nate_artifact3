#pragma once
class player
{
public:
	// class initialization
	player(int leftSide,int rightSide);
	
	// class member variables
	int chipNumber = 3;
	int toLeft;
	int toRight;

	// class functions
	int getLeft();
	int getRight();
	void setChipNumber(int chip);
	int getChipNumber();
};

