LCR Rules
Left Center Right (LCR) is a multiplayer dice game with three to five players. The goal is to win all of the chips.
------
Dice Rules
There are three dice rolled each turn. Each die has the letters L,C, and R on it along with dots on the
remaining three sides.
-For each L rolled the player must pass one chip to the player to their left.
-For each R rolled the player must pass one chip to the player to their right.
-For each C rolled the player must place one chip in the center pot and those chips are now out of play.
-Dots are neutral and require no action to be taken for that die.
------
The Chips
-Each player starts with three chips.
-If a player has only one chip, they roll only one die. If a player has two chips left, they roll two dice.
-If a player runs out of chips they are not out of the game but do not roll any dice on their turn.
------
Winning the Game
The winner of the game is the last player with chips.  
