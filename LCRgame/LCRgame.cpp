#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <time.h>
#include <stdlib.h>
#include <conio.h>
#include <Windows.h>
#include "player.h"
using namespace std;


void CoutCentered(std::string text)
{
	// This function will only center the text if it is less than the length of the console!
	// Otherwise it will just display it on the console without centering it.
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE); // Get the console handle.
	PCONSOLE_SCREEN_BUFFER_INFO lpScreenInfo = new CONSOLE_SCREEN_BUFFER_INFO(); // Create a pointer to the Screen Info pointing to a temporal screen info.
	GetConsoleScreenBufferInfo(hConsole, lpScreenInfo); // Saves the console screen info into the lpScreenInfo pointer.
	COORD NewSBSize = lpScreenInfo->dwSize; // Gets the size of the screen
	if (NewSBSize.X > text.size()) {
		int newpos = ((NewSBSize.X - text.size()) / 2); // Calculate the number of spaces to center the specific text.
		for (int i = 0; i < newpos; i++) cout << " "; // Prints the spaces
	}
	cout << text << endl; // Prints the text centered :]
}



int main()
{
	// initialize variables
	char playerInput;
	int diceNumber = 0;
	int diceRoll;
	int chipNumber;
	int roundNumber = 1;
	bool mainMenu = true;
	bool playerSelection = true;
	bool gameOver = false;

	// set up rng seed
	srand((unsigned)time(NULL));

	// loop through main menu until game starts
	while (mainMenu == true)
	{
		// clear screen and output menu options
		system("cls");
		CoutCentered("LCR Game");
		CoutCentered("1: Begin Game");
		CoutCentered("2: Game Rules");
		// take in users input
		playerInput = _getch();

		// the second option is chosen read rules from text file and output them
		if (playerInput == '2')
		{
			system("cls");
			string line;
			ifstream myfile;
			myfile.open("gamerules.txt");
			while (getline(myfile, line))
			{
				cout << line << endl;
			}
			myfile.close();

			// pause screen for player to read rules
			cout << endl;
			CoutCentered("Press any key to return to main menu.");
			playerInput = _getch();
		}

		// if first option is chosen initialize and start game loop
		else if (playerInput == '1')
		{
			mainMenu = false;
			system("cls");
			vector<player> players;
			while (playerSelection == true)
			{
				CoutCentered("Select number of players. Number must be between 3 and 5");
				playerInput = _getch();
				if (playerInput == '3')
				{
					player playerOne(2, 1);
					players.push_back(playerOne);
					player playerTwo(0, 2);
					players.push_back(playerTwo);
					player playerThree(1, 0);
					players.push_back(playerThree);
					playerSelection = false;
					break;
				}

				if (playerInput == '4')
				{
					player playerOne(3, 1);
					players.push_back(playerOne);
					player playerTwo(0, 2);
					players.push_back(playerTwo);
					player playerThree(1, 3);
					players.push_back(playerThree);
					player playerFour(2, 0);
					players.push_back(playerFour);
					playerSelection = false;
					break;
				}

				if (playerInput == '5')
				{
					player playerOne(4, 1);
					players.push_back(playerOne);
					player playerTwo(0, 2);
					players.push_back(playerTwo);
					player playerThree(1, 3);
					players.push_back(playerThree);
					player playerFour(2, 4);
					players.push_back(playerFour);
					player playerFive(3, 0);
					players.push_back(playerFive);
					playerSelection = false;
					break;
				}

				else
				{
					CoutCentered("Invalid Input, press the number key of the number of players you would like to have play");
					system("pause");
				}
			}
			

			// loop until the win condition happens
			while (gameOver == false)
			{
				// loop through players turns
				for (int i = 0; i < 3; ++i)
				{
					// output players chip numbers
					system("cls");

					// set current number of chips at start of turn to variable 
					chipNumber = players[i].getChipNumber();

					CoutCentered(("Round " + to_string(roundNumber)));
					cout << endl;
					CoutCentered("Player " + to_string(i + 1) + "'s turn");
					cout << endl;

					// if the player has atleast one chip add one dice to roll
					if (chipNumber > 0)
					{
						++diceNumber;
					}

					// if the player has atleast two chips add another dice to roll
					if (chipNumber > 1)
					{
						++diceNumber;
					}

					// if the player has 3 or more chips add another dice to roll
					if (chipNumber > 2)
					{
						++diceNumber;
					}

					CoutCentered("Number of Dice: " + to_string(diceNumber));
					cout << endl;

					CoutCentered("Current Number of Chips: " + to_string(players[i].getChipNumber()));
					cout << endl;
					CoutCentered("Press any key to roll the dice.");
					// if the player has no chips pass turn
					if (chipNumber == 0)
					{
						CoutCentered("You are out of chips, next player's turn.");
						cout << endl;
						CoutCentered("Press any key to end turn.");
						playerInput = _getch();
						continue;
					}

					playerInput = _getch();
					system("cls");
					CoutCentered(("Round " + to_string(roundNumber)));
					cout << endl;
					CoutCentered("Player " + to_string(i + 1) + "'s turn");
					cout << endl;

					// roll the dice the player has
					for (int dice = 0; dice < diceNumber; ++dice)
					{
						diceRoll = (rand() % 6) + 1;
						// if the die rolled a 1 pass one chip to the person on their left
						if (diceRoll == 1)
						{
							CoutCentered("You rolled a L, pass one chip to the player on your left.");

							// use class variable to find which player is on the left and add one chip to their total
							players[players[i].getLeft()].setChipNumber(players[players[i].getLeft()].getChipNumber() + 1);

							// remove 1 chip
							players[i].setChipNumber(players[i].getChipNumber() - 1);
						}

						// if the die rolled a 2 discard one chip
						else if (diceRoll == 2)
						{
							CoutCentered("You rolled a C, deposit one chip into the center discard pot.");

							// remove 1 chip
							players[i].setChipNumber(players[i].getChipNumber() - 1);
						}

						// if the die rolled a 3 pass one chip to the person on their right
						else if (diceRoll == 3)
						{
							CoutCentered("You rolled a R, pass one chip to the player on your right.");

							// use class variable to find which player is on the right and add one chip to their total
							players[players[i].getRight()].setChipNumber(players[players[i].getRight()].getChipNumber() + 1);

							// remove 1 chip
							players[i].setChipNumber(players[i].getChipNumber() - 1);
						}

						// if you roll a 4 - 6 output message
						else if (diceRoll == 4)
						{
							CoutCentered("You rolled a 4, your chips are safe.");
						}
						else if (diceRoll == 5)
						{
							CoutCentered("You rolled a 5, your chips are safe.");
						}
						else if (diceRoll == 6)
						{
							CoutCentered("You rolled a 6, your chips are safe.");
						}
					}
					cout << endl;
					CoutCentered("Number of chips after rolling: " + to_string(players[i].getChipNumber()));
					// reset number of dice to roll to 0
					diceNumber = 0;

					// check to see if player 1 has won
					if (players[1].getChipNumber() == 0 && players[2].getChipNumber() == 0)
					{
						// clear screen, output winning message and end game loop
						system("cls");
						CoutCentered("Player 1 wins!");
						gameOver = true;
						break;
					}

					// check to see if player 2 has won
					else if (players[0].getChipNumber() == 0 && players[2].getChipNumber() == 0)
					{
						// clear screen, output winning message and end game loop
						system("cls");
						CoutCentered("Player 2 wins!");
						gameOver = true;
						break;
					}

					// check to see if player 3 has won
					else if (players[0].getChipNumber() == 0 && players[1].getChipNumber() == 0)
					{
						// clear screen, output winning message and end game loop
						system("cls");
						CoutCentered("Player 3 wins!");
						gameOver = true;
						break;
					}
					cout << endl;
					CoutCentered("Press any key to end turn.");
					playerInput = _getch();
				}
				// increase round number
				++roundNumber;
			}
		}
		else
		{
			system("cls");
			CoutCentered("Invalid Input, press the 1 key to begin play or press the 2 key to see the rules.");
			CoutCentered("Press any key to return to main menu.");
			playerInput = _getch();
		}	
	}
}

