#include "player.h"
// initialize class variables
player::player(int leftSide, int rightSide)
{
	toLeft = leftSide;
	toRight = rightSide;
}

// function to get which player is on their left
int player::getLeft()
{
	return toLeft;
}

// function to get which player is on their right
int player::getRight()
{
	return toRight;
}

// function to set players chip number
void player::setChipNumber(int chip)
{
	chipNumber = chip;
}

// function to get players number of chips
int player::getChipNumber()
{
	return chipNumber;
}